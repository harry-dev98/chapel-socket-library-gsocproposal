NOTE: The below given library structure is to give a vague idea and may be  different from the actual library implementation.

Socket library has class `socket`.

`socket`: 
- `var sock: fd_t` - stores the socket file descriptor.
- `var sock_file: file` - stores the file
- `var AF_*: int` - all of these denoting address family, will be enums
- `var SOCK_*: int` - all of these denoting Socket Kind, will be enums
- `proc init(family=AF_INET, type=SOCK_STREAM, proto=0, fileno=none): socket` - this function will initalize a socket i.e. creates a socket descriptor. family is the address family used for the creating socket, e.g. AF_INET, AF_UNIX, type defines what type of socket is being created like a SOCK_STREAM(TCP) or SOCK_DATAGRAM(UDP) or SOCK_RAW, etc., proto is used to define what protocol will the socket use, fileno is the file descriptor which can be passed to get a socket object for the descriptor (it can be used to close a open socket connection for which we have just a file descriptor). So when the socket will be initialized its descriptor will be saved as a variable. It also calls `openfd` procedure of IO module on socketdescriptor and store it in `this.sock_file`.
- `proc bind(address: )` - 